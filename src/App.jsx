import './App.css'
import Counter from "./features/Counter/Counter";

function App() {
    return (
        <div className="App">
            <h1>The best counter example ever</h1>
            <Counter />
        </div>
    )
}

export default App

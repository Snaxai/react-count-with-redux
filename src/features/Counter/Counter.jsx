import { useDispatch, useSelector } from "react-redux";
import {
  actionCounterAttemptBoost,
  actionCounterDecrement,
  actionCounterIncrement,
} from "./counterState";

const Counter = () => {
  // Dispatch used to "trigger" actions
  const dispatch = useDispatch();
  // Get a portion of the state
  const counter = useSelector((state) => state.counter);

  // Event Handlers
  const handleIncrement = () => {
    dispatch(actionCounterIncrement());
  };
  const handleDecrement = () => {
    dispatch(actionCounterDecrement());
  };
  const handleBoost = () => {
    dispatch(actionCounterAttemptBoost(5));
  };

  return (
    <section>
      <h4 className="AppCount">Counter: {counter}</h4>

      <div className="AppActions">
        <button onClick={handleIncrement}>Increment</button>
        <button onClick={handleDecrement}>Decrement</button>
        <button onClick={handleBoost}>Boost</button>
      </div>
    </section>
  );
};

export default Counter;
